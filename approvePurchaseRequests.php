<?php
// Define an abstract class for the Approver in the chain.
abstract class Approver
{
    protected $nextApprover; // Reference to the next Approver in the chain
    protected $approvalLimit; // The maximum amount this Approver can approve

    public function setNextApprover(Approver $nextApprover)
    {
        $this->nextApprover = $nextApprover; // Set the next Approver in the chain
    }

    public function processRequest($amount)
    {
        if ($amount <= $this->approvalLimit) {
            $this->approveRequest(); // If the amount is within the limit, approve the request
        } elseif ($this->nextApprover !== null) {
            $this->nextApprover->processRequest($amount); // Pass the request to the next Approver
        } else {
            echo "Request denied by all approvers.\n"; // If no Approver can approve, deny the request
        }
    }

    abstract protected function approveRequest(); // Abstract method to be implemented by concrete Approvers
}

// Concrete Approver class for a Manager
class Manager extends Approver
{
    protected $approvalLimit = 1000; // Manager's approval limit

    protected function approveRequest()
    {
        echo "Manager has approved the request.\n"; // Approval action for Manager
    }
}

// Concrete Approver class for a Director
class Director extends Approver
{
    protected $approvalLimit = 5000; // Director's approval limit

    protected function approveRequest()
    {
        echo "Director has approved the request.\n"; // Approval action for Director
    }
}

// Concrete Approver class for a CEO
class CEO extends Approver
{
    protected $approvalLimit = 10000; // CEO's approval limit

    protected function approveRequest()
    {
        echo "CEO has approved the request.\n"; // Approval action for CEO
    }
}

// Client code
$manager = new Manager();
$director = new Director();
$ceo = new CEO();

$manager->setNextApprover($director); // Set up the chain of responsibility
$director->setNextApprover($ceo);

$purchaseAmount = 6000;
$manager->processRequest($purchaseAmount); // Process a purchase request